import React, { Component } from 'react';
import ExpeditionList from './components/ExpeditionList';
import SortedList from './components/SortedList';
import PopUp from "./components/PopUp";
import data from './data/data.json';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: data,
      seen: false,
      value_id: "",
      sortedArr: [],
      sorted: false
    }
  }

  sortByDate = () => {  
    const dateArr = Object.entries(data)
    const sortData = dateArr.sort(function(a,b){
      return new Date(a[1].date + " "+a[1].schedules[0][0]) - new Date(b[1].date + " "+b[1].schedules[0][0]);
    });

    const sortedId = sortData.map(([key, value]) => {  
      return value.id
    });
    
    this.setState({
      data: sortedId,  
      sorted: true,
      sortedArr: sortData
    })
  };
  

  togglePop = (e) => {
    const id = e.target.textContent; 
    this.setState({
      seen: !this.state.seen,
      value_id: id
    });
  };


  toggleSortDate = (e) => {
    this.sortByDate()
    this.setState({
      sorted: true,
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="Exped-id">
          <div className="insteadofh4">Expedition ID</div>
          {this.state.sorted ?
            
           	<SortedList 
              data={this.state.data}
              sortedArr={this.state.sortedArr}
            />
            :
            <ExpeditionList 
              data={this.state.data}
              sortByDate={this.toggleSortDate}
              togglePop={this.togglePop}
              value_id={this.state.value_id}
            />}
          </div>
          <div>
            {this.state.seen ? 
              <PopUp 
                data={this.state.data} 
                togglePop={this.togglePop}
                value_id={this.state.value_id}
                /> 
              : null}
          </div>
        </header>
      </div>
    );
  }
}


export default App;
