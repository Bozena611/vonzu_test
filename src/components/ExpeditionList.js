import React from 'react';
import SingleID from './SingleID';

export default function ExpeditionList (props) {
  if (props.data) {
  return (
  	<div>
    	{Object.entries(props.data).map(([key, value]) => {
    		return (
    			<SingleID 
    			key={key}
	    		data_id={value.id}
          togglePop={(e) => props.togglePop (e, value.id )}
          value_id={value.id}
    			/>
  			)
    	})}
    		<div>
          <button 
          data-testid="button-sort"
          style= {{cursor: "pointer"}} 
          onClick={props.sortByDate}>Sort</button>
        </div>
    </div>	    
  );
  } else {
    return null
  }
}


