import React from 'react';


export default function SortedList (props) {
  const sorted = props.sortedArr;
  if (sorted) {
  return (
  	<div>
    	{sorted.map((ele, i) => {
        return (
    			<div key={i}>
            <h4 style={{color: 'purple'}}>{ele[1].id}</h4>
            <p className="sortedDetails">Type: {ele[1].type}</p>
            <p className="sortedDetails">Date: {ele[1].date}</p>
            <p className="sortedDetails" style={{marginBottom:0}}>Time: {ele[1].schedules[0][0]} - {ele[1].schedules[0][1]}</p>
          </div>
    		)
    	})}
    	
    </div>	    
  )
} else {
  return null
};
}
