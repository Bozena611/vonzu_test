import React from 'react';


export default function SingleID (props) {
  return (
  	<div>
	  	<h5 
	  	data-testid="valueid" 
	  	style= {{cursor: "pointer"}} 
	  	onClick={props.togglePop}
	  	>{props.data_id}</h5>
    </div>
  )
}


