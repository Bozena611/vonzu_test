import React from 'react';
import ReactDOM from 'react-dom';
import PopUp from './PopUp';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PopUp />, div);
});