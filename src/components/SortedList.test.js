import React from 'react';
import { render } from '@testing-library/react';
import ReactDOM from 'react-dom';
import SortedList from './SortedList';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SortedList />, div);
});
