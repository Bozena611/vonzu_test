import React from "react";
import '../App.css';


export default function PopUp (props) {
  if (props.data) {
    return (
      <div>
        <div>
          <div className = "Details-flex">
            <div className="close">
              <span style= {{cursor: "pointer"}} onClick={props.togglePop}>
                &times;
              </span>
            </div>
            <div className="Pop-uph4">Click ID again or X to close</div>
            {Object.entries(props.data).map(([key, value]) => {
              if (value.id === props.value_id) {
                return (
                  <ul key='{key}' className = "detailsCard">
                    <li><span className="detailsBold">ID:</span> <span>{value.id}</span></li>
                    <li><span className="detailsBold">Type:</span> <span>{value.type}</span></li>
                    <li className="detailsBold">Address:</li> 
                    <div className="indentAddress">
                      <li><span className="detailsBold">Street:</span> <span>{value.address.street}</span></li>
                      <li><span className="detailsBold">Postal Code:</span> <span>{value.address.postalCode}</span></li>
                      <li><span className="detailsBold">City:</span> <span>{value.address.city}</span></li>
                      <li><span className="detailsBold">Province:</span> <span>{value.address.province}</span></li>
                      <li><span className="detailsBold">Country:</span> <span>{value.address.country}</span></li>
                    </div>         
                    <li><span className="detailsBold">Date:</span> <span> {value.date}</span></li>
                    <li><span className="detailsBold">Weight:</span> <span> {value.weight}</span></li>
                    <li><span className="detailsBold">Schedule:</span> <span>{value.schedules[0][0]} - {value.schedules[0][1]}</span></li>
                    <li><span className="detailsBold">Comments:</span> <span> {value.comments}</span></li>
                  </ul>
              )
              } else {
                return null
              }
            })}
          </div>   
        </div>
      </div>
    );
  } else {
    return null
  }
}

