import React from 'react';
import { render, cleanup } from '@testing-library/react';
import ReactDOM from 'react-dom';
import SingleID from './SingleID';

afterEach(cleanup);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SingleID />, div);
});


it('inserts text in h5', () => {
	const { getByTestId } = render(<SingleID data_id='00199821' />);
	expect(getByTestId("valueid")).toHaveTextContent("00199821");
});