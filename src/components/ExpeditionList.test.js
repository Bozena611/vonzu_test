import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import ReactDOM from 'react-dom';
import ExpeditionList from './ExpeditionList';


afterEach(cleanup);

const defaultProps = { 
  onClick: jest.fn(),
  text: "Sort" ,
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ExpeditionList />, div);
});

it('calls correct function on click', () => {
  const onClick = jest.fn();
  const { getByText } = render(<button {...defaultProps} onClick={onClick}>Sort</button>)
	//render(<SortedList />);
	fireEvent.click(getByText(defaultProps.text));
	expect(onClick).toHaveBeenCalled();
});